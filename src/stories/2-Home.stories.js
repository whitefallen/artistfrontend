import React, { Component } from "react";

export class Home extends Component {
  render() {
    return (
      <div>
        <h1>
          This is a Example Page for the Artist Frontend, to handle Crud
          Operations in ReactJS.
        </h1>
      </div>
    );
  }
}
