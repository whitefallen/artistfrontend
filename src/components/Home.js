import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import About from "./About";
import User from "./User";

export class Home extends Component {
  render() {
    return (
      <div>
        <div>
          <h1>
            This is a Example Page for the Artist Frontend, to handle Crud
            Operations in ReactJS.
          </h1>
        </div>
        <Router>
          <div>
            <nav>
              <ul>
                <li>
                  <Link to="/about">About</Link>
                </li>
                <li>
                  <Link to="/users">Users</Link>
                </li>
              </ul>
            </nav>
            <Switch>
              <Route path="/about">
                <About />
              </Route>
              <Route path="/users">
                <User />
              </Route>
            </Switch>
          </div>
        </Router>
      </div>
    );
  }
}

export default Home;
